# Generated by Django 3.1.1 on 2020-11-23 22:58

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('story_7', '0001_initial'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Item',
            new_name='AccordionItem',
        ),
    ]
