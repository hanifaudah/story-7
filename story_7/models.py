from django.db import models

class AccordionItem(models.Model):
  title = models.CharField(max_length=500)
  content = models.TextField()

  def __str__(self):
    return f'{self.title}: {self.content}'
